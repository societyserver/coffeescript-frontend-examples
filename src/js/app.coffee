app = angular.module 'app', [
	'LocalStorageModule'
	'steam-service'
	'realss.main'
	'ui.router'
	'ui.bootstrap'
	'ngSanitize'
]

app.config ['$urlRouterProvider', '$stateProvider', ($urlRouterProvider, $stateProvider) ->
	$urlRouterProvider
		.otherwise '/'
	
	$stateProvider.state '*path',
			url: '*path'
			templateUrl: 'partials/content.html'
			controller: 'ContentCtrl'
]
