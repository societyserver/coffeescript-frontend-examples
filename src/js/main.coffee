getUrl = (index, links) ->
	url = ""
	if (index+1 == links.length)
		return ""
	for value, i  in links
		if i <= index
			url += "/" + value
	return url

app = angular.module 'realss.main', []

app.controller 'ContentCtrl', ['$scope', '$rootScope', '$location', 'steam', (S, R, loc, steam) ->

	S.path = loc.$$path

	R.buildPath = () ->
		R.url = loc.$$path
		
		R.links = R.url.split("/")
		R.links.shift()
		
		R.paths = []

		for value, index in R.links
			R.paths[index] = {"name": R.links[index], "url": getUrl(index, R.links)}

	R.buildPath()


	R.activeTab = R.url.split("/")[1]

	R.activeTabInner = R.activeTab + "/" + R.url.split("/")[2]


	handleContent = (data) ->
		S.content = data.object.content
		#S.content = sce.trustAsHtml(data.object.content.replace(/<img /g, "<img steamimg "))

	steam.get("/home/realss/website"+loc.$$path+"/index.html").then(handleContent)

]

app.controller 'NavCtrl', ['$scope', '$rootScope', '$location', 'steam', (S, R, loc, steam) ->
	handleTree = (data) ->
		R.menu = data.navigation

	steam.get("/home/realss/website/tree").then(handleTree)
	
	R.isSelected = (tabName) ->
		return tabName == R.activeTab

	R.isSelectedInner = (tabName) ->
		return tabName == R.activeTabInner

	R.getName = (name) ->
		R.title = name;
]

app.directive('img', ->
	restrict: 'E',
	replace: false,
	link: (scope, elem, attr) ->
		if attr.src[0..6] != "http://" and attr.src[0..7] != "https://"
			if scope.path
				path = scope.path+"/../"
			else
				path = "/"
			#console.log("REALSS steamimg", scope.path, attr.src)
			attr.$set("src", "http://steam.realss.com/home/realss/website"+path+attr.src)
)

app.directive('dynamic', ($compile) ->
	restrict: 'A',
	replace: false,
	transclude: true,
	link: (scope, ele, attrs) ->
		scope.$watch(attrs.dynamic, (html) ->
			ele.html(html)
			$compile(ele.contents())(scope)
		)
)
